"use strict";

Template.home.helpers({
    postList: function () {
        return Posts.find({}).fetch();
    }
});

Template.home.events({
    "submit .message-form": function (event) {
        event.preventDefault();
        var name = Meteor.user().username;
        var message = event.target.chat.value;

        Posts.insert({
            author: name,
            chat: message,
            createdAt: new Date()
        });
    }
});